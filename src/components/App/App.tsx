import React, { FunctionComponent, useState } from "react";
import styled from "styled-components";
import Display from "../Display/Display";
import Pad from "../Pad/Pad";
import { Digit, Operator } from "../../lib/types";

const StyledApp = styled.div`
  font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto,
    "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji",
    "Segoe UI Symbol";
  font-size: 16px;
  width: 100%;
  max-width: 320px;
  width: 320px;
`;

//main logic with use of hooks
export const App: FunctionComponent = () => {
  // Calculator's states
  //result
  const [result, setResult] = useState<number>(0);
  //empty array string for history
  let initialArray = [""];
  const [history, setHistory] = useState(initialArray);

  const [waitingForOperand, setWaitingForOperand] = useState<boolean>(true);
  const [pendingOperator, setPendingOperator] = useState<Operator>();
  //initial display
  const [display, setDisplay] = useState<string>("0");

  const calculate = (
    rightOperand: number,
    pendingOperator: Operator
  ): boolean => {
    let newResult = 0;
    if (pendingOperator === "+5" || pendingOperator === "-5") {
      newResult = Number(display);
    } else {
      newResult = result;
    }

    switch (pendingOperator) {
      case "+":
        newResult += rightOperand;
        break;
      case "-":
        newResult -= rightOperand;
        break;
      case "+5":
        newResult += 5;
        setWaitingForOperand(true);
        setPendingOperator(undefined);
        break;
      case "-5":
        newResult -= 5;
        setWaitingForOperand(true);
        setPendingOperator(undefined);
        break;
    }

    setResult(newResult);
    setDisplay(newResult.toString().toString().slice(0, 12));
    if (pendingOperator === "+5" || pendingOperator === "-5")
      setHistory((oldArray) => [
        ...oldArray,
        `${rightOperand} ${pendingOperator} = ${newResult}`,
      ]);
    else {
      setHistory((oldArray) => [
        ...oldArray,
        `${result} ${pendingOperator} ${rightOperand} = ${newResult}`,
      ]);
    }

    return true;
  };

  // Pad buttons handlers
  const onDigitButtonClick = (digit: Digit) => {
    let newDisplay = display;

    if ((display === "0" && digit === 0) || display.length > 12) {
      return;
    }

    if (waitingForOperand) {
      newDisplay = "";
      setWaitingForOperand(false);
    }

    if (display !== "0") {
      newDisplay = newDisplay + digit.toString();
    } else {
      newDisplay = digit.toString();
    }

    setDisplay(newDisplay);
  };

  const onPointButtonClick = () => {
    let newDisplay = display;

    if (waitingForOperand) {
      newDisplay = "0";
    }

    if (newDisplay.indexOf(".") === -1) {
      newDisplay = newDisplay + ".";
    }

    setDisplay(newDisplay);
    setWaitingForOperand(false);
  };

  const onOperatorButtonClick = (operator: Operator) => {
    const operand = Number(display);
    if (operator === "+5" || operator === "-5") {
      calculate(operand, operator);
    } else {
      if (typeof pendingOperator !== "undefined" && !waitingForOperand) {
        if (!calculate(operand, pendingOperator)) {
          return;
        }
      } else {
        setResult(operand);
      }

      setPendingOperator(operator);
      setWaitingForOperand(true);
    }
  };

  const onChangeSignButtonClick = () => {
    const value = Number(display);

    if (value > 0) {
      setDisplay("-" + display);
    } else if (value < 0) {
      setDisplay(display.slice(1));
    }
  };

  const onEqualButtonClick = () => {
    const operand = Number(display);

    if (typeof pendingOperator !== "undefined" && !waitingForOperand) {
      if (!calculate(operand, pendingOperator)) {
        return;
      }

      setPendingOperator(undefined);
    } else {
      setDisplay(operand.toString());
    }

    setResult(operand);
    setWaitingForOperand(true);
  };

  const onAllClearButtonClick = () => {
    setResult(0);
    setPendingOperator(undefined);
    setDisplay("0");
    setWaitingForOperand(true);
    setHistory([]);
  };

  const onClearEntryButtonClick = () => {
    setDisplay(display.slice(0, -1));
  };

  return (
    <div>
      <StyledApp>
        <Display
          value={display}
          expression={
            typeof pendingOperator !== "undefined"
              ? `${result}${pendingOperator}${waitingForOperand ? "" : display}`
              : ""
          }
        />
        <Pad
          onDigitButtonClick={onDigitButtonClick}
          onPointButtonClick={onPointButtonClick}
          onOperatorButtonClick={onOperatorButtonClick}
          onChangeSignButtonClick={onChangeSignButtonClick}
          onEqualButtonClick={onEqualButtonClick}
          onAllClearButtonClick={onAllClearButtonClick}
          onClearEntryButtonClick={onClearEntryButtonClick}
        />
        {history.length > 1 ? (
          <table className="styled-table">
            <thead>
              <tr>
                <th>History</th>
              </tr>
            </thead>
            <tbody>
              {history
                .filter((item) => item)
                .reverse()
                .map((entry) => (
                  <tr key={entry}>
                    <td>{entry}</td>
                  </tr>
                ))}
            </tbody>
          </table>
        ) : (
          <h4>No History Available.</h4>
        )}
      </StyledApp>
    </div>
  );
};

export default App;
