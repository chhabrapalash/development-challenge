# Calculator in React and TypeScript

The calculator has been built using Typescript and React as part of Viveo Developer Group Challenge.
The code is divided into the following components:

## Components

| Name        | Functionality                                              |
| ----------- | ---------------------------------------------------------- |
| App.tsx     | Entry point for the program, wrapper for all the functions |
| Button.tsx  | Contains logic for digit buttons                           |
| Pad.tsx     | Contains all the operations for the calculator             |
| Display.tsx | Provides the display screen                                |

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## How to Run the Project

In the project directory, you first need to run:

### `npm install`

This will install all required dependencies. The use the following

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

OR

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
